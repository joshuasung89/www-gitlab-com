---
layout: handbook-page-toc
title: "Fast decisions — Managing so Everyone Can Contribute (MECC)"
canonical_path: "/handbook/mecc/fast-decisions/"
description: Fast decisions — Managing so Everyone Can Contribute (MECC)
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

{::options parse_block_html="true" /}

<%= partial("handbook/mecc/_mecc_overview.erb") %>

# Fast Decisions

Fast decisions are a key attribute of [MECC](/handbook/mecc/). While other management philosophies prioritize the speed of knowledge ***transfer***, MECC optimizes for the speed of knowledge ***retrieval***.  

Below are the tenets of Fast Decisions within MECC. 

## Iteration

In conventional organizations, there's often inherent pressure to present a complete and polished project, document, or plan. This expectation slows progress and expends valuable time that could be used to exchange multiple rounds of feedback on smaller changes. 

A key aspect of MECC is incorporating [iteration](/handbook/values/#iteration) into every process and decision with a [low level of shame](/handbook/values/#low-level-of-shame). This means doing the smallest viable and valuable thing, and getting it out quickly for feedback. Despite the initial discomfort that comes from sharing the [minimal viable change (MVC)](/handbook/values/#minimal-viable-change-mvc), iteration enables faster execution, a shorter feedback loop, and the ability to course correct sooner. 

This philosophy mirrors the GitLab product from a cycle time standpoint. GitLab is built to reduce the time between making a decision and getting the result to market. Iteration enables cycle time reduction to be applied in day-to-day decision making.

**Here's an example**: [Iterating on promotional videos to launch MECC](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/digital-production/-/issues/319)

In the development of MECC, our team at GitLab aspired to produce [two high-quality videos](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/digital-production/-/issues/319) to introduce an accompanying MECC Certification to GitLab team members as well as the wider community. When it became clear that MECC's brand identity would be bolstered in the coming weeks through an agency partnership, we decided to produce only one video — an example of [Minimal Viable Change (MVC)](/handbook/values/#minimal-viable-change-mvc) — and iterate as new information came in. This showcases a maturity in embracing iteration. It celebrates the [boring solution](/handbook/values/#boring-solutions) (one video, the minimum required to inform GitLab team members), and enabled faster decisions throughout the launch phase. 

To empower even more of your team to make fast decisions through iteration, consider hosting [Iteration Office Hours](https://www.youtube.com/watch?v=2eA7-E950ps). 

## Short toes 

An organization's speed of decision making can be dramatically slowed if teams are concerned about "stepping on others' toes" by contributing to work outside of their immediate job description. This is often fueled by a fear of conflict, which is one of the [five dysfunctions](/handbook/values/#five-dysfunctions) of a team.

Adopting a MECC mentality means having [short toes](/handbook/values/#short-toes) and feeling comfortable with others taking initiative to contribute to your domain. Eliminating a territorial mindset allows for better [collaboration](/handbook/values/#collaboration), more [diversity of thought](/handbook/values/#seek-diverse-perspectives), and ultimately faster decisions.  

**Here's an example**: [Marketing team member contributing a proposal to improve a People function process](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/issues/555)

In a People Group Conversation at GitLab, the following question was asked: "*What can we do from a company side to make sure people aren’t overworking?*" At conventional organizations, people outside of the People Group may not risk "stepping on their toes" by proposing iterations and solutions. At GitLab, [a proposal was offered](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/issues/555) by a team member in Marketing. This proposal added an automated message within Slack to remind people to consider taking time off, and to add the note to their next manager 1:1 if they felt that they could not take PTO. Following a healthy discussion, the proposal was [added to GitLab's handbook](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/37624) and implemented in its tool stack. 


## Proposals

Traditional organizations may use synchronous meetings to host brainstorming sessions. Research shows that these session have many flaws, including [production blocking](https://hbr.org/2022/02/why-virtual-brainstorming-is-better-for-innovation). MECC unlocks faster decision making by defaulting to asynchronous brainstorming, which requires an individual to [make a proposal](/handbook/values/#make-a-proposal) first. This empowers others to brainstorm when they are ideally suited to contribute, and the forcing function of writing (or sharing verbal thoughts via a platform like Yac or Loom) generates [clarity](https://twitter.com/AdamMGrant/status/1551208238581948416). 

In a MECC organization, [everything is in draft](/handbook/values/#everything-is-in-draft) by default. Establishing a shared understanding that everything is subject to change removes red tape and [politics](/handbook/values/#playing-politics-is-counter-to-gitlab-values) and creates an environment where teams can ship smaller changes, faster.  

**Here's an example**: [Support Engineering documenting a proposal to catalyze an async brainstorm](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/3072)

GitLab's Support Engineering team articulated a thought during a sync session, centered on the exploration of automatically assigning tickets to active team members. Rather than gathering a large number of colleagues in a Zoom call to brainstorm, a team lead [created a GitLab issue](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/3072) and asked others to contribute brainstorm ideas there. This led to faster decisions on captured ideas, including several that were deemed not a priority. The documented brainstorm serves future decisions as well. By capturing context, teams can avoid rehashing concepts and make faster (future) decisions. 


## Directly responsible individual (DRI) 

Another foundational element to MECC is that each project or decision is assigned a [directly responsible individual (DRI)](/handbook/people-group/directly-responsible-individuals/) who is directly responsible for its success or failure. This prevents the risk of sluggish decision-making that conventional organizations often experience when there are unclear expectations and too many people involved. This is achieved [combining the best of both hierarchical and consensus organizations](/company/culture/all-remote/management/#separating-decision-gathering-from-decision-making).

Leaders must foster a culture where DRIs are empowered, able to [escalate to unblock](/handbook/values/#escalate-to-unblock), and willing to share their ideas in the open. This unlocks the team's highest potential. A successful DRI should consult and collaborate with all teams and stakeholders and welcome input from a broad range of diverse perspectives as they form their thoughts. 

It's important to note that MECC still allows flexibility for team members to [disagree, commit, and disagree](/handbook/values/#disagree-commit-and-disagree), but it reduces the risk that disagreement or dissent will prevent a [bias for action](/handbook/values/#bias-for-action). 

**Here's an example**: [Learning & Development team member owns decisions related to her result metrics](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/24)

A member of GitLab's Learning & Development team was responsible for [developing mental health awareness content](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/24). Given that she was the one doing the work, and her result metric was the one impacted, she was given latitude to be the Directly Responsible Individual. This enabled her to make fast decisions about content type and structure, as opposed to waiting for a more senior person to sign off or appoint her as the lead for this piece of work. 


## Informal communication to build trust

An intentional approach to [informal communication](/company/culture/all-remote/informal-communication/) is crucial in a fast-paced organization with a bias for [asynchronous workflows](/company/culture/all-remote/asynchronous) and [text-based communication](/company/culture/all-remote/effective-communication/). Leaders should encourage team members to prioritize informal connections (e.g. coffee chats, social calls, special interest chat channels) and [get to know the people](/handbook/values/#get-to-know-each-other) behind the text. This builds trust, prevents conflict, and enables better communication during work-related interactions. 

Building this level of trust also helps enable DRIs to make faster decisions, as there's a foundation of confidence in the experience and judgment of others. 

**Here's an example**: [Sharing READMEs (personal operating manuals) to build trust with new team members](/handbook/marketing/readmes/dmurph/)

Two GitLab team members have never worked together before, so they set up a [coffee chat](/company/culture/all-remote/informal-communication/#coffee-chats) and exchange [READMEs](/handbook/marketing/readmes/dmurph/) prior to a new project starting. They learn a lot about each other, their work styles, and their backgrounds in the 25-minute video call and the asynchronous README reviews prior. The project runs more smoothly because of their [shared trust](/handbook/leadership/building-trust/) beyond the transactional work interactions.

## Two-way door decisions

In MECC, decisions are [two-way doors](/handbook/values/#make-two-way-door-decisions), meaning they're easy to reverse. That's why a DRI should go ahead and make the decision without approval or consensus. The only time a decision should require a more thorough discussion first is when you can't reverse it ***or*** break it down into smaller, reversable components. 

This requires a reframing of the conventional management mindset. Reverting work back to a previous state is a positive thing, because you're quickly getting feedback and learning from it. Making a small change quickly prevents a bigger revert in the future. 

**Here's an example**: [Support decision is shipped and then reverted, leading to an async brainstorm](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/4485)

Engineers at GitLab collaborated on a change to clarify that GitLab Community Edition can have support. Following discussion in the [merge request](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/merge_requests/765), it was merged into production. Hours later, a member of GitLab's Support Team [reverted the change](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/merge_requests/838) with context on why. This two-way door decision enabled faster decision making. Even though we returned through the same door we entered (hence, two-way), it hastened decision making on the topic by generating an [async brainstorm issue](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/4485) following the reversion.  

---

Fast decisions enable [Many decisions](/handbook/mecc/many-decisions), the third tenet of [MECC](/handbook/mecc/). 

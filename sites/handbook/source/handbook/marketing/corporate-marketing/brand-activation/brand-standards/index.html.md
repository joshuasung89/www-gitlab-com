---
layout: handbook-page-toc
title: "Brand Resources"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Brand Resources
{:.no_toc}

### Overview
Our brand is the embodiment of our [mission](https://about.gitlab.com/company/mission/#mission), [vision](https://about.gitlab.com/direction/#vision), and [values](https://about.gitlab.com/handbook/values/). As stewards of the [GitLab brand](/company/brand/), the [Brand Design](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-design/) team works to educate and enable the wider organization with resources to effectively and honestly communicate what the company does for our internal and external audiences.

### Brand guidelines
GitLab's official Brand guidelines can be found at **[design.gitlab.com](https://design.gitlab.com/)** under the `Brand` tab. These guidelines should be applied to all marketing materials, including, but not limited to: digital ads, events, publications, merchandise, etc. 


### Brand oversight

Occasionally the [old GitLab logo](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/design/gitlab-brand-files/gitlab-logo-files/full-color/solid-logo/png/gitlab-logo-gray-rgb.png) is still in use on partner websites, diagrams or images, and within our own documentation. If you come across our old logo in use, please bring it to our attention by creating an issue in the [Marketing](https://gitlab.com/gitlab-com/marketing/general/issues) issue tracker. Please include a link and screenshot (if possible) in the description of the issue and we will follow-up to get it updated. Thanks for contributing to our brand integrity!


### Brand expressions vs sub-brands

`Brand expressions` and `sub-brands` are extensions of our core brand. They are both governed by our [brand guidelines](https://design.gitlab.com/brand/overview), but take on additional creative elements that complement the core branding. Brand expressions help distinguish projects and initiatives that directly promote either the company or the product. Sub-brands are used to classify GitLab programs that bring awareness to topics that extend beyond the company and product.

#### Brand Expression Strategy
Brand expressions are directly aligned with GitLab marketing initiatives and connect back to the company and/or product. Visually, they lean into GitLab core branding, while maintaining their own specific look and feel.

An example of a brand expression is the [Partner Leadership Summit](https://twitter.com/hapapower/status/1529106372771577857). This brand expression builds upon the core branding by incorporating new elements, such as the distinct arrow motif, that make it stand apart from our standard marketing collateral.

#### Sub-branding Strategy
Sub-brands are established for approved initiatives that generate awareness, but don't explicitly connect back to our product. They typically have their own marketing strategy but can be included in ongoing initiatives as well. Sub-brands embrace the guidelines of the core GitLab brand (visually and tonally), while incorporating unique additional creative elements (i.e. a distinct logo, alternative illustration style, etc.). Sub-brands should reflect GitLab's core branding and not feel like a complete departure from it. 

An example of a sub-brand is [Level Up](https://levelup.gitlab.com/), GitLab's certification program. While Level Up offers technical training for the product, it has a broader focus on DevOps as a whole, which extends beyond the company and product.




### Quick links

**Logo files**
- [Press kit](https://about.gitlab.com/press/press-kit/) 

**Color palette**
- [RGB swatches](https://drive.google.com/file/d/1Ihb3DiRcm94KegtPFR3yXeTkXsoeGNJL/view?usp=sharing)
- [CMYK swatches](https://drive.google.com/file/d/1jHEZsVcdw4i-qJlgcCTfLBDn6XS08eVx/view?usp=sharing)

**Marketing illustrations** 
- [Illustration library](https://drive.google.com/drive/folders/1GLoJ1Ua55vqTcYVfobcRunu9r7mQiLDq?usp=sharing)
- [RGB swatches, secondary palette](https://drive.google.com/file/d/1kCcvxYMKPkDCEFQd6imQcHhFGC14Hgte/view?usp=sharing)
- [CMYK swatches - secondary palette](https://drive.google.com/file/d/1J2ZutCXZPJHQc9fgHvJYaVFSkOtlyn4t/view?usp=sharing)

**Marketing icons** 
- [Icon library](https://drive.google.com/drive/u/1/folders/1dsRceA94H8CI0q7JAeWwEuWoNUuqdGq-)
- [Software development lifecycle](https://drive.google.com/drive/u/1/folders/15kLTAKngeVEE7dWbP471EVKfhF25kSnN)

**Photography**
- [Photo library](https://drive.google.com/drive/folders/1VHErs-KSNX1FIIVgXJR3OmIzwU7M4E1M?usp=sharing)
- [Adobe stock](https://stock.adobe.com/)

**Templates** 
- [GitLab pitch deck template](https://docs.google.com/presentation/d/1vtFnh8DU6ZZzASTHg83UrhM6LJWqo5lq9mJDAY2ILpw/edit?usp=sharing)

**Additional resources**
- [Brand guidelines](https://design.gitlab.com/)
- [Corporate Marketing handbook](https://about.gitlab.com/handbook/marketing/corporate-marketing/)
- [Brand Design handbook](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-design/)



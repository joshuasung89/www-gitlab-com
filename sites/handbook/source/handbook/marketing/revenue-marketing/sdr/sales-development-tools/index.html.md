---
layout: handbook-page-toc
title: "Sales Development Tools"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Sales Development Tools

This page is to act as your guide to the many tools and best practices that the Sales Development Organization uitlizes.

## Salesforce

[Salesforce](https://www.salesforce.com/) is our CRM of record. It integrates with all of the other applications in our business tech stack. Salesforce stores prospect, customer, and partner information. This includes contact information, products purchased, bookings, support tickets, and invoices, among other information.

### Salesforce Training Resources

* GitLab Edcast: [Salesforce Intro Class](https://gitlab.edcast.com/journey/weeks)
* GitLab Edcast: [Advanced Training: Working Leads](https://gitlab.edcast.com/journey/week)
* Best Practices: [IQMs and Opportunities](https://gitlab.edcast.com/journey/week-note)

### Lead and Contact Views

#### SDR Lead Views

* **S1 View** - [MQL’s](https://about.gitlab.com/handbook/marketing/marketing-operations/marketo/#mql-and-lead-scoring) and Hot leads that require quick follow up (i.e event)s, and Drift conversation leads for tenured SDRs
* **S2 View** - Only leads that are active in a HT touch sequence and have a phone number
* **S3 View** - Qualifying leads. These are leads that you are actively qualifying in a back and forth conversation either by email or through phone calls.  Each lead needs to either be active in a follow-up sequence, have an active task, or have a future meeting scheduled which can be seen in a future “last activity” date.

#### BDR Lead Views

* **B1 View** - [MQL’s](https://about.gitlab.com/handbook/marketing/marketing-operations/marketo/#mql-and-lead-scoring) and Hot leads that require quick follow up 
* **B2 View** - Leads associated with “Actively Working” accounts where you are the BDR Assigned
* **B3 View** - Needs to be sequenced. They’re leads you chose to move into your name but they have not yet been sequenced
* **B4 View** - Active HT sequenced leads that have a phone number - to help with call downs
* **B5 View** - Qualifying leads. These are leads that you are actively qualifying in a back and forth conversation either by email or through phone calls. Each lead needs to either be active in a follow up sequence, have an active task or have a future meeting scheduled which can be seen in a future “last activity” date


#### BDR Contact Views

**Contact ownership is based on the BDR Assigned and Account owner fields. If you are the BDR Assigned on the account, you will be the owner of all contacts associated with that account. If there is no BDR Assigned, the account owner will be the owner of the contacts.**

* **B1 View** - Includes MQL’s, Hot contacts that require quick follow up 
* **B2 View** - Contacts related to Actively working accounts that you can choose to sequence - *give 48 hrs
* **B3 View** - Active HT sequenced leads that have a phone number - to help with call downs
* **B4 View** - Qualifying leads. Contacts that you are actively qualifying in a back and forth conversation either on email or through phone calls. Each contact in this status needs to either be active in a follow up sequence, have an active task or have a future meeting scheduled which can be seen in a future “last activity” date.

## Outreach

[Outreach.io](https://www.outreach.io/) is a tool used to automate emails in the form of sequences. Users can track open rates, click-through rates, response rates for various templates, and update sequences based on these metrics. Outreach.io also helps to track sales activities such as calls. All emails/calls/tasks that are made through Outreach.io are automatically logged in Salesforce with a corresponding disposition. See below for a list of current call dispositions, what they mean, and scenarios on when to use each of them.

### Outreach Training Resources

* [Intro Slide Deck](https://docs.google.com/presentation/d/1IpTVE4-Nkblfuiu6f1OUnFbi8IpjRPK7UDSx4eSfQCg/edit#slide=id.g5a343b482a_2_10) and [Training Video](https://gitlab.edcast.com/journey/weeks/cards/7257647)
    * password should be in onboarding issue or ask your manager
* GitLab Edcast: [Advanced Outreach Training](https://gitlab.edcast.com/journey/weeks)
* Best Practices: [Our Outreach Handbook Page](https://about.gitlab.com/handbook/marketing/marketing-operations/outreach/)

### Outreach Sequences

 A user created series of touchpoints (calls, emails, LinkedIn) in order to communicate with a prospect and automate the user's workflow

* **Primary Sequence:** a sequence created by SDR leadership that should be used by all reps to follow up with inbound leads
* High touch and low touch sequences: a high touch sequence should be used for higher-quality leads. High touch sequences require you to add in more personalization and have more touch points across a longer period of time, including phone calls and LinkedIn Connect requests. Low touch sequences are typically automated and run for a shorter period of time. We define higher quality leads based on the volume of inbound you have to manage as well as whether the lead has has 1-2 of the following for MM/Large accounts:
    * Valid company email domain or company name
    * Phone number
    * Title related to our buying/influencer personas
    * Demandbase intent
    * Set up for company/team use checkbox equals true
    * Account has X # of Developers (MM/Large nuances)
    * LinkedIn profile
        * SMB lead may need to have 2-3 of the above1-2 of the above
        * Different teams may have nuances so please check in with your manager for their feedback.

### Outreach Collections

Ways to organize similar sequences and snippets

* **Common Collections**
    * [FY23 Inbound High Touch Collection](https://app1a.outreach.io/sequences?direction=desc&order=recent&content_category_id%5B%5D=44)
    * [FY23 Inbound Low Touch Collection](https://app1a.outreach.io/sequences?direction=desc&order=recent&content_category_id%5B%5D=45)
    * [FY23 Inbound Languages Collection](https://app1a.outreach.io/sequences?direction=desc&order=recent&content_category_id%5B%5D=46)
    * [Events Collection](https://app1a.outreach.io/sequences?direction=desc&order=recent&content_category_id%5B%5D=22)

### Outreach Tags

A method of distinguishing sequences and snippets from others. Use tags to help narrow down which sequences you should be using in a particular situation

#### Common Tag Examples:

* **Primary-** a sequence created by SDR leadership that should be used by all reps to follow up with inbound leads
* **High Touch/Low Touch-** Distinguish if the sequence is high or low touch
* **Region-** Can be used to show which region the sequence 
* **GTM-** The message trying to be delivered. Ex. Security, CI/CD ect. 
* **Inbound/Outbound-** Whether the sequence is meant for inbound or outbound prospecting 
* **Language-** What language the sequence is written in

### Outreach Snippets

Content created for BDRS and SDRs to use to create hyper-personalized sequences, one-off emails, or to use for reaching out to prospects via LinkedIn.

* **Common Snippet Examples**
     - [Objection Snippets](https://app1a.outreach.io/snippets?direction=asc&order=owner&tags%5B%5D=objection)
     - [Support Snippets](https://app1a.outreach.io/snippets?direction=asc&order=owner&tags%5B%5D=Support)

## ZoomInfo

[Zoominfo](https://www.zoominfo.com/) provides our Sales Development Representatives and Account Executives with access to hundreds of thousands of prospects and their contact information, company information, tech stack, revenue, and other relevant data. Individual records or bulk exports can be imported into Salesforce using extensive search criteria such as job function, title, industry, location, tech stack, employee count, and company revenue. More information can be found on the Marketing Operations [Zoominfo handbook page.](https://about.gitlab.com/handbook/marketing/marketing-operations/zoominfo/)

### ZoomInfo Training Resources 

* ZoomInfo University: [40 Minute Introduction Video](https://university.zoominfo.com/learn/course/101-introduction-to-zoominfo-on-demand/training-session/101-introduction-to-zoominfo)
* GitLab Edcast: [ZoomInfo Introduction Training](https://gitlab.edcast.com/journey/week)  
* GitLab Edcast: [ZoomInfo Advanced Training](https://gitlab.edcast.com/journey/week-note) 
* ZI 101 Video: [Quick Saved Searches](https://www.youtube.com/watch?v=OpTgvoOQ1jM)
* ZI 101 Video: [Leveraging List Match](https://www.youtube.com/watch?v=vl1kpsNa874)
* ZI 101 Video: [Tagging Prospects in LinkedIn](https://www.youtube.com/watch?v=GQWTZWbMgg8) 
* GitLab Handbook: [Zoominfo handbook page](https://about.gitlab.com/handbook/marketing/marketing-operations/zoominfo/)

## Linkedin Sales Navigator

[LinkedIn Sales Navigator](https://www.linkedin.com/sales/login) is a prospecting tool & extended reach into LinkedIn Connections.

### LinkedIn Training Resources

* LinkedIn Intro: [70 Minute Navigator Tutorial](https://www.linkedin.com/learning/learn-linkedin-sales-navigator-3/welcome-to-sales-navigator)
* GitLab Edcast: [LinkedIn Navigator Training](https://gitlab.edcast.com/journey/week-note)
* GitLab Video: [Peer Tips](https://drive.google.com/file/d/1xzz8cEiSFqZ7bOw-dpqoNlHjSDwrIFE4/view)
* GitLab Video: [LinkedIn Personalization](https://www.youtube.com/watch?v=7vEVB-WgDPA)
* GitLab Video: [Loading Accounts from Salesforce into Linkedin](https://www.youtube.com/watch?v=_D8walmmOAU) 

## Demandbase

Demandbase is a targeting and personalization platform that we use to reach our different audiences based on intent data and our ideal customer profiles. [Demandbase Handbook Page](https://about.gitlab.com/handbook/marketing/account-based-marketing/demandbase/)

### Demandbase Training Resources

* GitLab Video: [50 Minute XDR Demandbase Training Video](https://www.youtube.com/watch?v=R3uwFAMhsHM)
* GitLab Slides: [Demandbase Platform Training](https://docs.google.com/presentation/d/1qJNxl503zO0x5UuUz-TOe5Lt_jWQa_Hgz-UrNCQqjOk/edit#slide=id.g29a70c6c35_0_68) 
* GitLab Handbook: [Demandbase Handbook Process Page](https://about.gitlab.com/handbook/marketing/account-based-marketing/demandbase/)
* GitLab Video: [Creating an Account List Video](https://www.youtube.com/watch?v=BVccN6ly2ys&feature=youtu.be)

## Chorus

Call and demo recording software. [Chorus](https://www.chorus.ai/) tracks keywords, provides analytics, and transcribes calls into both Salesforce and Outreach. Chorus will be used to onboard new team members, provide ongoing training and development for existing team members, provide non-sales employees with access to sales calls, and allow sales reps to recall certain points of a call or demo. At this time, US Sales, US Customer Success, US SDRs will be considered recorders. Non-US Commercial and Non-US SDRs can request recorder access once they have completed the GDPR training course. Non-US recorders will also need access to the EMEZ Zoom Group. Everyone else can access the tool as a listener if they wish.

### Chorus Training Resources

* Chorus: [Tips for Getting Start](https://docs.chorus.ai/hc/en-us/articles/115009183547-Tips-on-Getting-Started-with-Chorus)
* GitLab Edcast: [Chorus Overview](https://gitlab.edcast.com/journey/week)
* GitLab Edcast: [Sample Chorus IQM Calls](https://gitlab.edcast.com/insights/sample-chorus)

## Drift

[Drift](https://www.drift.com/) is a conversational marketing platform (live chat tool). We use the chat platform Drift to engage site visitors. Additional information on best practices can be found on the [Drift Handbook Page.](https://about.gitlab.com/handbook/marketing/marketing-operations/drift/#best-practices)

### Drift Training Resources

* GitLab Training: [50 Minute Drift Intro Training Video](https://drive.google.com/drive/u/0/folders/1aGbQopASH0y540by0BS4UQQkjAL9bvyD)
* GitLab Edcast: [GitLab’s Drift Work Flows Videos](https://gitlab.edcast.com/journey/week-note) 
* GitLab Best Practices: [Drift Best Practices Document](https://docs.google.com/document/d/1EQYVu2cO-Zn1T2EuTQrIG4z-pRZucs7FG0rhQQo4wzs/edit) 

## LeanData

When a lead is created in Salesforce, [LeanData](https://www.leandatainc.com/) will be the tool that routes it to the appropriate user. Routing rules include sales segmentation, region, lead source, and owned accounts. For example, if a lead from a named account is created, it will be routed directly to the owner of the named account. Also, LeanData provides cross-object visibility between leads and accounts, and contacts. When in an account record, a user can view ‘matched’ leads by company name, email domain, and other methods.

### LeanData Training Resources

* GitLab Handbook: [LeanData Handbook Page and Workflow](https://about.gitlab.com/handbook/marketing/marketing-operations/leandata/) 

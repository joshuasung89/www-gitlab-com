---
layout: handbook-page-toc
title: SA Career Development
description: >-
  For career development Solution Architects can choose between an individual
  contributor or leadership track
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[**SA Practices**](/handbook/customer-success/solutions-architects/sa-practices/) - [**Sales Plays**](/handbook/customer-success/solutions-architects/sales-plays/) - [**Tools and Resources**](/handbook/customer-success/solutions-architects/tools-and-resources/) - [**Career Development**](/handbook/customer-success/solutions-architects/career-development/) - [**Demonstration**](/handbook/customer-success/solutions-architects/demonstrations/) - [**Processes**](/handbook/customer-success/solutions-architects/processes/) - [**Education and Enablement**](/handbook/customer-success/education-enablement/)

## Career Development

Solutions Architecture team members you should review the [GitLab People Group Career Development page](/handbook/people-group/learning-and-development/career-development/) for the roles, responsibilities, and helpful career development resources.

### Solution Architecture Specific Career Development

#### Career Paths

- Solution Architects can progress through the [Solution Architect Individual Contributor Levels](https://about.gitlab.com/job-families/sales/solutions-architect/#solutions-architect).
- Solutions Architects can also pursue a leadership role and progress through the [Solution Architect Manager Levels](https://about.gitlab.com/job-families/sales/solutions-architect/#manager-solutions-architects).
- Solutions Architects have also moved laterally throughout the GitLab organization (e.g., Marketing, Product, Professional Services, etc.).
- Solutions Architects can move to similar roles outside of GitLab. Because of the valuable experience and skills gained while helping to drive customer outcomes, Solutions Architects can move to a variety of technical leadership roles across industries.

#### Promotion Process

The Solutions Architecture promotion process augments the [People Group Promotion Process](/handbook/people-group/promotions-transfers/) with the following:

1. Before the SA Manager starts the [BambooHR Promotion Approval Process](/handbook/people-group/promotions-transfers/#bamboohr-promotion-approval-process), the manager will inform the Sr. Director of Solutions Architecture that the justification document is ready for review, typically during a 1 on 1 weekly meeting.
    - For Staff and Principal level promotions, the Sr. Director of SA will consult with the SA leadership team, typically during a weekly team meeting. This is to ensure continuous calibration for Staff and above-level roles that have lower representation amongst the SA organization.
    - For Staff level promotions, the Sr. Director of SA will consult with existing Staff and Principal level SAs for peer-level review and feedback.
    - For Principal level promotions, the Principal candidates will prepare and present their accomplishments and their vision for improving GitLab and the SA organization to a panel of SA leaders and Principal level SAs.
    - The Principal promotion panel will then meet to discuss the candidate's suitability for the Principal SA role.
1. After reviewing and providing feedback, the Sr. Director of Solutions Architecture will inform the VP of Customer success, typically during a weekly 1 on 1 meeting.
1. The VP of Customer Success will review the request and provide feedback if necessary. If the request is approved, the VP will annotate the justification document with their approval.
1. At this point, the SA manager may submit the request in Bamboo.

### Associate Solution Architecture Program

In FY23, GitLab kicked off its first ever Associate Program across Customer Success with the aim to:

- Build the next generation of highly-diverse, well-prepared GitLab Solutions Architects 
- Grow pre-sales engineering skill sets that are focused on GitLab’s product offering, business model and aligned to customer needs
- Give existing SAs an opportunity to grow in their careers through having early-in-career SAs to mentor and coach

The program leverages the enablement material that all SAs use in the onboarding process in addtion
to providing team members earlier in their careers content that instills a passion for a single DevOps Platform built on open source.  

The program helps build technical, trusted advisory muscle memory, places team members in a role serving the SMB segment, and in a function where there is 
high awareness of priorities that are aligned to visible results.

#### How to Get Engaged with the Program!

**If you are looking to coach or mentor Associate SAs** that are going through the program you can do so by helping the Associate SAs understand
the value of different [GitLab categories within our stages](/handbook/product/categories/#hierarchy). Upon completion of Demo2Win, every week, the team is assigned a new Category to learn about and
show the value of that category through the tell-show-tell methodology. Sign up for updates by including your GitLab username as a CC recipient to the 
[Weekly Capability Deep Dive](https://gitlab.com/-/ide/project/gitlab-com/customer-success/solutions-architecture/associate-program/training/tree/main/-/.gitlab/issue_templates/Weekly%20Capability%20Deep%20Dive.md/) issue template which is used to determine the weekly category.

Additionally, the team is finalizing a mentoring program that is slated to begin in Q3 of FY23. 

**If you are looking to be considered for the Associate SA Program**, look at our Job Board for any Associate SA postings.

## Recognition

Recognizing the hard work that the Solutions Architects bring to the sales process and to GitLab itself is something we hold dear.  The SA organization should feel proud of the wins they achieve.  Whether they are business related or personal goals reached, SA's deserve to be called out for their accomplishments.  

### Peer Recognition
Recognizing (and being recognized) by your peers is extremely fulfilling.  If an SA is recognized by a peer, it may appear in one or more of these places:

- The #thanks Slack Channel (Used by the entire organization for all manner of appreciation.)
- The #deal-gratitude Slack Channel (Used primarily by the [Field Organization](https://about.gitlab.com/handbook/sales/field-operations/field-enablement/field-team-recognition/#deal-gratitude-slack-channel) for recognizing work on sales deals.)
- The #wins-key-deals Slack Channel (When an SA is associated with a high profile win, they are automatically added to the details of the win within this channel. SA Leadership is working with Sales Ops to bring more attention to this notification.)
- Various team meetings (At the start of most SA calls, there is a devoted section for expressing gratitude and appreciation.)
- [Discretionary Bonuses](https://about.gitlab.com/handbook/incentives/#discretionary-bonuses-for-individuals) (If an SA has gone above and beyond and truly lived up to GitLab's CREDIT values, a discretionary bonus of $1000 US can be awarded. These nominations can come from anyone in the company and at any time.)



### Management Recognition
At various points of the year, SA's have the opportunity to be recognized by their managers and/or Customer Success leadership. 

- [Field Organization Quarterly Achievers](https://about.gitlab.com/handbook/sales/field-operations/field-enablement/field-team-recognition/#field-quarterly-achievers-chat) (12 various winners from accross the Field Organization for outstanding performance in the Quarter. A designated chat with the CEO, CRO and the other winners as well as a $250 US expensed dinner are included) 
- [President's Club](https://internal-handbook.gitlab.io/handbook/sales/presidents-club/) (8 SA's from across the company are selected based on sales contributions throughout the year. More on the selection criteria can be found in the internal link)

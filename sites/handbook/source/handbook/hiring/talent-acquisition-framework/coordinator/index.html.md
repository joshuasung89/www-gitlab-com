---
layout: handbook-page-toc
title: "Candidate Experience Specialist Responsibilities"
description: "This page is an overview of the processes, and systems that the Candidate Experience Team is responsible for."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Candidate Experience Specialist Responsibilities
{: #framework-coord}

The CES team utilizes [GitLab Service Desk](/stages-devops-lifecycle/service-desk/) to track all incoming requests to the CES team. If you are a GitLab team member and you have a request or question for the Candidate Experience team, email `ces@gitlab.com` and a ticket will automatically be created within the CES Service Desk. Please see the [alignment page](https://about.gitlab.com/handbook/hiring/recruiting-alignment/#recruiter-coordinator-and-sourcer-alignment-by-department) to learn more about which CES team member will be working on your requests.

### Best Practices

- The CES team focuses their attention on incoming requests in the following order: CES Service Desk, individual emails, `@ces` Slack pings, and lastly Slack direct messages.
- You can find the priority list based on the type of request below:
   - 0 - Contracts
   - 1 - Candidate emails
   - 2 - Reschedules
   - 3 - Schedule Interviews
   - 4 - Background checks and Offboarding
- The CES team utilizes a program called [Guide](https://about.gitlab.com/handbook/hiring/talent-acquisition-framework/coordinator/#using-the-candidate-guide) to enhance the candidate experience and create a more transparent view into the talent acquisition processes. This program also allows us to rename interview subject lines to make them more candidate-friendly and succinct.

#### How the CES Team will run the Service Desk

1. Under this [CES Service Desk Project](https://gitlab.com/gl-talent-acquisition/ces-service-desk) set up the proper notifications
   - Click on the bell icon on the top right next to Star and Clone
   - Go to Custom Settings
   - Check "New issue"
   - Close out the window
1. On the left-side menu bar click Issues
   - This is where all our incoming CES emails will create an issue. You'll get an alert when someone sends an email to the CES email alias. Any "emails" that need to be addressed will be an open issue listed within this project.
1. Each CES will begin their workday by triaging issues within the Service Desk based on the [identified priority list](https://about.gitlab.com/handbook/hiring/talent-acquisition-framework/coordinator/#best-practices) by adding the appropriate label to the issue and then will begin working on Level 0 priority requests and so on.
   1. Click on the new Issue
   1. Assign it to yourself on the right-side toolbar
   1. Read the Issue message
   1. If you need to respond to the "email" by adding comments to the issue, be sure to enter comments as you would an email to the candidate. Always assume that a candidate could be included in any email.
   1. If no action is needed and the issue has been solved, you may close it. Do not close issues until you have resolved the problem you are solving.

### Screening

No action is needed by the CES team on this step. For additional information:
- Once a candidate is moved into the Screening stage, the candidate is automatically sent the GSIS.
- When scheduling a candidate for a screening call with a recruiter, the Calendly link is to be used. We will no longer use the "Request Availability" option for screening calls as this creates unnecessary manual work
   - If the candidate is sourced, the sourcer should send the Calendly
   - If it's an agency candidate or a referral where the recruiter may see the candidate first, the recruiter should send their Calendly link

### Team Interviews

- When a candidate is ready for a team interview, the recruiter will request availability in Greenhouse using the email address of the CES team member [they partner with](https://about.gitlab.com/handbook/hiring/recruiting-alignment/#recruiter-coordinator-and-sourcer-alignment-by-department). They will then click "Email the Team" on the right side of the candidate's Greenhouse page and send CES* the "CES Scheduling Request form".
   - If there are shorter timelines for roles as defined by the hiring team, CES needs to be made aware of those timelines for when interviews need to be scheduled. We will default to scheduling interviews at a minimum of [48 hours](https://about.gitlab.com/handbook/hiring/interviewing/#moving-candidates-through-the-process) out to leave time for interviewer prep.
   - Reminder: When tagging `@ces` in Greenhouse, do not tag anyone other than the team in the same ping as it will add those users to the Service Desk project.
- If no response within 48 hours, CES will send a follow up request for availability and set the due date of the issue to 24 hours after the next email to check for availability again.
   - After the 2nd email to candidate with no response, the CES will make the Recruiter aware of the no response and no longer attempt to receive availability. They can close the issue temporarily, and reopen if the Recruiter receives a response from the candidate.
- After availability is received, CES will schedule interviews based on the interview plans in Greenhouse and will track preferences within the CES Service Desk [repo](https://gitlab.com/gl-talent-acquisition/ces-service-desk/-/tree/master).
   - CES will be using the Zoom integration with [Prelude](https://about.gitlab.com/handbook/hiring/interview-schedule/) for interviews
   - After the candidate has provided the availability, as an optional step the CES team members can send the Candidate First Touch Email from Greenhouse if there are scheduling challenges and are pausing on sending the Guide for 24 hours. 
 - This email is sent to inform the candidates that their interview scheduling is in the process and to ensure better engagement with the candidates.
- If CES sees an alert to merge applicant profiles, and those profiles are a match (candidates' email address, phone number, or resume matches), they will merge the applicant profiles. Visit [People Technology and Operations page](/handbook/hiring/talent-acquisition-framework/talent-acquisition-operations-insights/#system-processes) for instructions.
- CES will [send interview confirmations via Guide](https://about.gitlab.com/handbook/hiring/talent-acquisition-framework/coordinator/#using-the-candidate-guide)
- CES will not automatically schedule next interviews based off of scorecards
   - The Recruiter or Hiring Manager will need to follow the same scheduling procedures as above in Greenhouse to request scheduling assistance on the next round of interviews.
   - Any candidates who receive a no or definitely not scorecard should be discussed with the Recruiter and the Hiring Manager before declining.

#### Executive Interview Scheduling

The process for scheduling executive interviews should always be followed from [here](https://about.gitlab.com/handbook/eba/#interview-requests-with-executives).

If you have any questions about the process, please post in #eba-team Slack channel and @ mention the correct EBA. You should include a link to the #eba-team Slack channel thread in your issue so you know where to find updates.

#### Prelude

Please ensure you have the [Prelude Chrome Extension](https://chrome.google.com/webstore/detail/interview-schedule/nbimjaonnklighojgkihkipemiaimgdk) installed on your Chrome Browser.

Prelude processes can be found [here](https://about.gitlab.com/handbook/hiring/prelude/).

#### Resource - Candidate Guide

Please ensure you have the [guide Chrome extension](https://chrome.google.com/webstore/detail/resource-button/ikeemflbkbdhkgmgbdpeapmnnggbfogd) installed on your Chrome Browser.

Guide processes can be found [here](/handbook/hiring/resource-guide/).

#### If a Candidate Withdraws from the Hiring Process

If a candidate emails the CES Service Desk stating they would like to withdraw from the interview process, the CES should respond to the candidate's email (not the Service Desk ticket) and cc the recruiter (if the recruiter isn't already on the email). The CES response should not say anything definite as there may be a chance for the recruiter to reel the candidate back in. A basic response is below:

> Hi `Candidate Name`, thank you for your email. I have cc'd your recruiter in this email and if there are any additional questions, they will be in touch.

The CES should also tag the Recruiter in the Greenhouse profile of the candidate who withdrew.

The only exception to this should be for executive roles. Please just forward the withdrawal email to the Executive Recruiter and they will handle any follow-up.

### Non-Disclosure Agreements

To send a Non-Disclosure Agreement (NDA) to a candidate, Recruiters should ping @ces in Greenhouse.
CES can then log into Docusign, select "start" and "use a template", and then access shared templates.
Once the NDA has been returned, upload the document in the "details" section of Greenhouse. Download this document from DocuSign as separate files, and only use the NDA itself (the summary is not necessary).

### Background Checks

GitLab will obtain references and complete a criminal [background check](https://about.gitlab.com/handbook/people-policies/#background-checks) with employment verifications.

   - If the candidate is located in Belgium and the role is not Grade 9 or higher, the CES or Recruiter (whoever kicks off the reference check process) should send the job requisition to the legal team via the #legal Slack channel during Reference/Background check phase to determine whether or not the role is a position of trust.
- The recruiter should select "Email the team" and send the "CES Start Contract Request" email to CES* to initiate the offer letter and background check processes after the verbal offer has been made.
- The Candidate Experience Specialist will [initiate a background check](/handbook/people-policies/#background-checks) and start the contract process for the candidate. CES will continue to monitor the background check until finalized, using the follow-up feature in Greenhouse to ensure the background check is complete and uploaded into BambooHR, if hired
- Background check results will be received by the Candidate Experience Specialist and brought to the relevant People Business Partner for adjudication
- Candidates in **Canada** require a form to be filled out by the employer (GitLab). The candidate will send the form with 2 forms of approved ID (listed on the form) and the CES will verify the name on each form of ID, enter it on the background check form. The CES will enter their signature on the form (either by editing a PDF or staging in DocuSign) and send back to the candidate.
- Employment Verification results will be reviewed and checked against LinkedIn profiles or CVs by the Candidate Experience Specialist and any discrepancies will brought to the relevant People Business Partner

For additional information on reviewing Background checks see [this page](/handbook/people-policies/#background-checks).

#### Initiating a Background Check through Greenhouse

**US Candidates Only**

1. Log in to [Greenhouse](https://app2.greenhouse.io/dashboard) and go to the candidate's profile.
1. Click the `Private` tab.
1. Click `Export to Sterling`.
1. Click `Complete Report`, which will redirect you to the Sterling website.
1. Scroll down and click `Add Screening`.
1. Next to `Comprehensive Criminal with Employment`, click on `Ticket`.
1. If you need to run a financial check as well for Finance team members, you will need to submit a second ticket.
      1. After you submit a ticket for the comprehensive criminal check, navigate back to the candidate's SterlingONE profile.
      1. Click `Add Screening`.
      1. Next to `Federal Criminal District Search` click `Ticket`.
1. Check off that you agree to your obligations as a user.
1. Under `Disclosure and Authorization Options`, select the first option to have Sterling send the candidate a disclosure form.
1. Click `Generate Ticket`.
1. Make a note in the Greenhouse profile that the Background Check has been started

#### Initiating a Background Check through Sterling Talent Solutions

**Non-US Candidates Only**

1. Log in to [Sterling](https://secure.sterlingdirect.com/login/Default.aspx) and E-invite the candidate by inputting their email address.
1. Under "Applicant Information" enter in the candidate's first and last name, as well as their email address to confirm.
1. Next, select "GitLab" from the "Job Position" drop down menu.
1. Next, select "A La Carte" from the "Screening Packing".
1. After that, you will select "Criminal-International". A drop down menu will appear, and you will select the country the candidate is located in. Then click "Add". Do this for any country other than Japan, Russia, Germany, Ireland, or the Netherlands (details below).
1. You'll then select "Verification-Employment (International") and click "Add".
1. If you are submitting a background check for a candidate located in Japan, Germany, Ireland, or Russia **or if you need to run a financial check for Finance team candidates**, you will select `Extended Global Sanctions` instead of "Criminal-International". Then click "Add"
1. If you are submitting a background check for the Netherlands, please skip the criminal international check and proceed only with employment verification. The candidate will be completing a VoGS/Certificate of Good Conduct with HRSavvy instead.
1. Make sure each of the checks you have requested are listed in the "Search" box.
1. Finally, scroll to the bottom of the page and click "Send"
1. Make a note in the Greenhouse profile that the Background Check has been started

**Non-US Candidates Only (New Sterling Dashboard View)**

1. Login to [Sterling](https://sterlingcheck.app/dashboard/#/dashboard/) and click `Submit Invite` on the lefthand side toolbar.
1. `Account` and `Workflow` will auto-populate with Gitlab,Inc -SD and Consent Plus.
1. On the Position dropdown, select `GitLab`.
1. Next, select `à la carte` from the Screening Package dropdown.
1. Next, select `Criminal-International` from the Add-Ons dropdown (can search in the search bar), and push TAB to populate the "Jurisdiction for Criminal-International" box on the righthand side.
   - If you are submitting a background check for a candidate located in Japan or Russia, **or if you need to run a financial check for Finance team candidates**, you will select `Extended Global Sanctions` instead of "Criminal-International".
   - If you are submitting a background check for the Netherlands, do not order a `Criminal-International` check and move on to the next step.
1. Next, select `Verification-Employment (International)` from the Add-Ons drowpdown.
1. Next, select the candidate's country from the dropdown on the righthand side under `Add-On Details`.
1. Next, enter in the candidate's location under `Location of Employment`.
1. Click `Next` at the bottom.
1. Enter in the candidate's First name, Last name, and Email.
1. Click `Submit`.

#### Escalating Criminal Charges or Employment Discrepancies

Once the CES team gets the background checks back, if there are criminal charges or any employment verification discrepancies - the CES team will escalate to the Team Member Relations team.

1. Gather all details you can from the Sterling report
1. Post all details in the shared Slack channel with the PBP's `employment-criminal-escalations` with PDF version of the report
   - Make sure to include candidate's name, the job applied for, and start date.
1. The Team Member Relations Partners have their own criteria/process for providing their recommendation on how to proceed to the business
1. Once they has come to a recommendation, they will share the details of that recommendation with us and the Hiring Manager, if appropriate.
   - It is ultimately up to the business on whether they would like to proceed with the candidate
1. If approval to move forward is given, we will upload the approval to the BambooHR profile once created
1. If approval is not given to move forward, the CES will loop in the Recruiter to make sure the decision and why is communicated correctly to the candidate

### Speaking with TMRG team members in the hiring process

Our hiring process includes an **optional** step where candidates can request to meet with a TMRG team member. We will offer this to a candidate when they complete their interview process before an offer is made. Whether or not the candidate decides to take us up on this offer will have no impact on our overall hiring decision.

All current TMRGs have agreed to take part in this process. You can find the group list [here](https://about.gitlab.com/company/culture/inclusion/erg-guide/).

When a candidate requests to meet with a TMRG team member, the Candidate Experience Specialist will do the following:
1. Reach out to TMRG volunteers for this call in the dedicated #candidate_tmrg-call slack channel.
    - Select Shortcuts button - looks like a plus sign with lightening bolt
    - Fill in appropriate details
       - Which TMRG would this canidate like to speak with?
       - Where is the candidate located?
       - What role is this candidate interviewing for
       - Click Submit
2. Once submitted, the request will ask volunteers to reply with their Calendly link and LinkedIN Profile (optional) for us to share with the candidate.

3. Once a volunteer has been found the Candidate Experience Specialist will email the candidate the 'TMRG Opportunity' email template in Greenhouse. The CES will update the template with the Team Members name, Calendly Link and LinkedIN profile if provided and then send to candidate.

3. If a volunteer has not been found within 24 hours the CES team member will reach out to the TMRG in their dedicated TMRG Slack Channel.

4. If a volunteer still has not been found within 24 hours of the second request, the CES will reach out to the [TMRG lead(s)](/company/culture/inclusion/erg-guide/) and request assistance with scheduling. If a volunteer has not been found within 3 business days of the request, the CES will ask the TMRG lead to take part in the conversation.

As a GitLab team member taking part in these calls, we advise you to start with a short introduction to you and your role here at GitLab. From here, we advise you to let the candidate lead the conversation as the goal is for you to answer their questions and offer insight into how we work.

These calls don’t require you to submit a scorecard in Greenhouse. If a candidate mentions something that you see as a red flag (e.g. they outline a past action of theirs that goes against our values) or shares something that would help us set them up for success, we advise you to share the details of this with the hiring manager for the role they’re interviewing for. It will be the responsibility of the Hiring Manager to review this and decide whether we need to alter the hiring or offer process for the candidate.


### Send contract

[See Candidate Experience Specialist Contract Processes section of the handbook](/handbook/hiring/talent-acquisition-framework/ces-contract-processes)

The [Candidate Experience Specialists](/job-families/people-ops/candidate-experience/) will prepare the contract. While the Candidate Experience Specialist will prioritize a contract above other tasks, the expected turn around on the task is 1 business day. If the contract is time-sensitive, please provide context for the rush. If the Candidate Experience Specialist cannot meet the 1 business day they will inform the recruiter and CES manager via Greenhouse and will provide context.

Recruiters should make themselves familiar with the basic knowledge of the contract processes that can be found on the [CES Contract Processes](https://about.gitlab.com/handbook/hiring/talent-acquisition-framework/ces-contract-processes/#framework-coord) page and the [Contracts, Probation Periods & PIAA](/handbook/people-group/contracts-probation-periods/#employment-agreements) page.

   1. Check all aspects of the offer:
      - Do we have the new team members' legal name in their profile?
         -  _It is extremely important to enter the team member's full legal name (as much as it is known before visually seeing a legal ID). Full legal name should be provided for the [background check](/handbook/people-policies/#background-checks) process. It is important to be as accurate as possible, including the person's full legal name as well as any accents used for their name.
      - Is the new team members' address listed on the details page?
      - What contract type and entity are required based upon location and offer details?
      - Is it clear how many (if any) RSUs this person should receive?
      - Is all necessary information (start date, salary, location, etc.) up to date?
      - Is the start date one that is not a "no start date" and provides an adequate amount of time for People Experience to have one weeks' notice?
      - Has the signatory been determined by the Candidate Experience Specialist and updated?
      - Has the Entity been selected based on the New Hire's location? If the job title has Federal or PubSec included, please check with the Recruiter if they set up the contract as Inc rather than Federal LLC to confirm.
   1. [Generate the contract within Greenhouse](/handbook/hiring/talent-acquisition-framework/ces-contract-processes) using a template based on the details found in the offer package.
   1. Contact the recruiter or new team member to gather any missing pieces of information (note: the address can be found on the background check information page).
   1. The Signatory will be either the Talent Acquisition Manager, VP of Talent Acquisition, Chief People Officer, or the CES for PEOs. This can be determinted by the Candidate Experience Specialist sending a message to the Contracts to Sign channel in Slack.
   1. Ensure that, if the contract was created outside of Greenhouse, the contract has been reviewed and approved by the Senior Director of Legal Affairs or a Total Rewards Analyst.
   1. [Stage the contract in DocuSign from within Greenhouse](/handbook/hiring/talent-acquisition-framework/ces-contract-processes), which emails the contract to the signing parties, with the recruiter, talent acquisition manager, and the hiring manager cc'd. It will be sent to the designated signatory as previously determined in Offer Details.
   1. When the contract is signed by all parties, the Candidate Experience Specialist will verify that the start date in Greenhouse is correct.
     - Ensure the candidate has completed the PIAA section with either a `Yes` and the specific details **or** `None`. (Its important that it is not just a GitLab or Github link and more specific info.)
     - If the candidate has specified a `Yes` and the specific details, make the People Connect Team aware in our private group Slack channel to kick off approval process.
   1. **Before marking the candidate as hired** the Candidate Experience Specialist will reject the candidate from any other active roles including roles for which they are a prospect (without sending a rejection email). _NOTE: If this step is skipped, the profile will not be exported to Workday when the candidate is marked as hired._
   1. **Before marking the candidate as hired** on all candidates, the CES will make sure that the profiles were merged (for internal candidates, this will be indicated if the candidate shows their original `Hired` inactive role). [View candidate merge instructions](/handbook/hiring/talent-acquisition-framework/talent-acquisition-operations-insights/).  _NOTE: If this step is skipped, it will create a duplicate profile exported to Workday when the candidate is marked as hired._
   1. **Before marking the candidate as hired** the CES will ping the recruiter and give them 24 hours to contact/reject all other active candidates. Once this is complete, the CES can proceed with hiring in GH (if candidates are still present in req, select the "keep open" option when setting candidate to hired. This will trigger a new opening to ensure candidates are still present in req)
   1. **Before marking the candidate as hired** the CES will verify if the listed Recruiter and Coordinator in the `Details` >  `Source & Responsibility` section of the candidate's profile is correct to ensure accuracy in reporting.
   1. The CES will mark the candidate as "Hired" in Greenhouse: _when prompted, select the option to close the req._ Please note, the new hire's Workday profile will be generated automatically.
   1. The Candidate Experience Specialist will send an email to total-rewards@gitlab with any variations in contract language (for example a draw). Compensation will sync with Payroll and Sales Ops for any necessary notifications on payment types.
   1. For internal hires ONLY - the CES will send the automated email template labeled "Internal Hiring Survey". You can also cancel the survey that will appear automatically when the candidate is marked as hired.
   1. The Candidate Experience Specialist will email the new team member the Welcome Email from Greenhouse with a cc to IT Ops, the Hiring Manager and the Recruiter.  For new team members in USA, use 'GitLab Welcome - US only' template.  For team members located outside the US, use 'GitLab Welcome - non US' template
      * Instructions on the [Notebook Ordering Process](/handbook/business-ops/team-member-enablement/onboarding-access-requests/#laptops) are included with this email.
   1. Should the start date change after the welcome email is sent please see the required steps [here](/handbook/hiring/talent-acquisition-framework/ces-contract-processes/#how-to-update-a-start-date-after-the-contract-is-signed).
   1. Exception to the start date and onboarding date alignment: If a new team member requires a specific start date for legal reasons (cannot have break in employment) but onboarding on that specific day is restricted (because of Public Holiday, etc.), the Candidate Experience Specialist can notify the People Connect Team in the private Slack channel `people-exp_ces`. The Contract, Greenhouse and Workday should reflect the same start date regardless of the actual onboarding date.  

It is **important** that the Candidate Experience Specialist notifies the People Connect Team of any important changes regarding the new team member, which also ensures the new team members are handed off properly to the People Connect Team.

### Next Steps

People Experience Associate will create the onboarding issue and start the [onboarding tasks](/handbook/people-group/general-onboarding/onboarding-processes/) no later than one week before the new team member joins. Should a contract not be signed prior to 5 working days from the start date, a new start date will be required.

For questions about the new team member's onboarding status, you can @mention them in the `#peopleops-confidential` Slack channel.

For questions about the new team member's laptop, ping [IT Ops](#it-ops) in Slack. If the questions arise through email, forward the email to itops@gitlab.com and ping IT Ops in #it-ops Slack, and @it-ops-team too due to volume.

### Interview Reimbursement Process

For candidates requesting [interview reimbursment](/handbook/hiring/interviewing/#reimbursement-for-interviewing-with-gitlab) the CES team will partner with the Accounts Payable (AP) team to ensure requests are processed confidentially and in a timely manner. AP and the CES team utilize [GitLab Service Desk](/stages-devops-lifecycle/service-desk/) to track incoming emails to the Interviews@gitlab.com email.

Under the [Interview Reimbursement Service Desk](https://gitlab.com/interview-reimbursement/ap-ces/issues) set up the proper notifications
   - Click on the bell icon on the top right next to Star and Clone
   - Go to Custom Settings
   - Check "New issue"
   - Closeout the window

Additional process details can be found on the [project README page](https://gitlab.com/interview-reimbursement/ap-ces/blob/master/README.md).

### Employment Offboarding

When employees are offboarding, People Experience will create an offboarding issue. Candidate Experience is responsible for completing the CES section under "Recruiting Operations". Once these steps have been completed, unfollow the issue to disable notifications.

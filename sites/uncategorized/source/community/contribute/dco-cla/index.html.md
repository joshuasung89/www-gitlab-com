---
layout: markdown_page
title: "Contributions: the GitLab DCO & CLA"
description: "Learn more about the DCO or CLA which apply to contributions to GitLab."
canonical_path: "/community/contribute/dco-cla/"
---

## All contributions subject to the DCO or the CLA 
All contributions to GitLab are subject to the [Developer Certificate of Origin](https://docs.gitlab.com/ee/legal/developer_certificate_of_origin.html#developer-certificate-of-origin-version-11) (DCO), or the [Corporate](https://docs.gitlab.com/ee/legal/corporate_contributor_license_agreement.html) or [Individual](https://docs.gitlab.com/ee/legal/individual_contributor_license_agreement.html) Contributor License Agreement (CLA), depending on where you're contributing and on whose behalf.

#### Which agreement applies and when?
- Individual and Corporate contributions to **GitLab CE** are subject to the [DCO](https://docs.gitlab.com/ee/legal/developer_certificate_of_origin.html#developer-certificate-of-origin-version-11).
- Individual contributions to **GitLab EE** are subject to the [Individual CLA](https://docs.gitlab.com/ee/legal/individual_contributor_license_agreement.html).
- Corporate contributions to **GitLab EE** are subject to the [Corporate CLA](https://docs.gitlab.com/ee/legal/corporate_contributor_license_agreement.html).

Learn more about the [difference between GitLab CE and GitLab EE](https://about.gitlab.com/install/ce-or-ee/), and our [commitment to open source stewardship](/company/stewardship/).

## Accepting the DCO or the CLA
By contributing to GitLab, you are deemed to have accepted the agreement (DCO, or Corporate or Individual CLA) applicable to your contribution.

#### Need a Corporate CLA covering all contributors on behalf of your organization?
To enter into an overarching Corporate CLA that covers all contributions made on behalf of a corporate contributor, reach out to `cla_managers@gitlab.com`.





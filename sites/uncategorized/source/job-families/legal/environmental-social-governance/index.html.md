---
layout: job_family_page
title: Environmental & Social Governance (ESG)
---

{add overview; a brief description of the job family}

## Responsibilities
* {add a bulleted list of responsibilities that apply for all levels of the role}

## Requirements
* Ability to use GitLab
* {add a bulleted list of requirements that apply for all levels of the role}

## Levels
### {add name of level - i.e. Junior/Senior/Manager. Note we do not list the intermediate level in the title but after the role in parentheses - i.e. IT Systems Engineer (Intermediate)}
The {add the position title} reports to the {add the reporting position and link to the job family or heading (if on the same page)}.

#### {add Level} Job Grade
The {add the role name} is a [grade #](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### {add Level} Responsibilities
* {add a bulleted list of responsibilities that apply for this level}

#### {add Level} Requirements
* {add a bulleted list of requirements that apply for all levels of the role}

#### {add Level} Performance Indicators
* {add 3-5 KPIs that this role will be the DRI for, if the PIs are the same for all levels remove this section and use the heading 2 section later in the template}

### {add name of level - i.e. Junior/Senior/Manager. Note we do not list the intermediate level in the title but after the role in parentheses - i.e. IT Systems Engineer (Intermediate)}
The {add the position title} reports to the {add the reporting position and link to the job family or heading (if on the same page)}.

#### {add Level} Job Grade
The {add the role name} is a [grade #](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### {add Level} Responsibilities
* {add a bulleted list of responsibilities that apply for all levels of this job family}

#### {add Level} Requirements
* {add a bulleted list of requirements that apply for all levels of this job family}

#### {add Level} Performance Indicators
* {add 3-5 PIs that this role will be the DRI for, if the PIs are the same for all levels remove this section and use the heading 2 section later in the template. PIs should link to the data}

## Segment
### {add name of Segment}
{add a brief description of the specialty}

#### {add name of Segment} Requirements
* {add a bulleted list}

#### {add name of Segment} Responsibilities
* {add a bulleted list}

## Specialties
### {add name of Specialty}
{add a brief description of the specialty}

#### {add name of Specialty} Requirements
* {add a bulleted list}

#### {add name of Specialty} Responsibilities
* {add a bulleted list}

## Performance Indicators
* {add 3-5 PIs that this role will be the DRI for, if PIs were listed for each level, because the are all unique, remove this section. PIs should link to the data}

## Career Ladder
{add brief description of the career ladder}

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.

*{add a bulleted list of the hiring process steps here}

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
